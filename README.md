---

## To-do List web app

This project has a backend and frontend repositories:

1. https://bitbucket.org/catcat/bunnytodo_backend/src/master/
2. https://bitbucket.org/catcat/bunnytodo_frontend/src/master/

For the backend I used *python* and the front is in *react-hooks.*
The url to check the app running is: **http://44.238.173.193:3000/**

The block diagram is in the backend repo in the [/structure.png](https://bitbucket.org/catcat/bunnytodo_backend/src/master/structure.png) path.



## Description
The app has one page in which you can see a user list with action buttons, where you can update or delete a user and view/add tasks.
When you click on any of the buttons, a modal popups.

**Update button:** It shows the name of the selected user, the input is enabled to edit and clicking on the submit button saves the change.

**Delete button:** Asks if you are sure you want to the delete the selected user.

**View/Add Task button:** If the user does not have a task then it shows a *add new task* button where you can enter the task description and mark if it has been done.
on the other hand if the user has tasks it shows the users' tasks with an update button for each row to change the description or marked as done or to-do. It also has the *add new task* button.

