from functools import wraps
from django.utils.decorators import available_attrs
from django.http import HttpResponseBadRequest


def require_get_params(params):
    def decorator(func):
        @wraps(func, assigned=available_attrs(func))
        def inner(request, *args, **kwargs):
            if not all(param in request.GET for param in params):
                return HttpResponseBadRequest(
                    'You should send all params: %s' % params
                )
            return func(request, *args, **kwargs)
        return inner
    return decorator