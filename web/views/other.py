from django.shortcuts import render
from rest_framework.decorators import api_view
from django.http import JsonResponse
from bunnytodo_backend.utils.decorators import require_get_params
from web.helpers.other_helper import OtherHelper

@api_view(['GET'])
@require_get_params(['initial_date', 'end_date', 'unit'])
def get_other(request):
    initial_date_param = request.query_params.get('initial_date', None)
    end_date_param = request.query_params.get('end_date', None)
    unit_param = request.query_params.get('unit', None)
    return JsonResponse(OtherHelper.get_other(
        initial_date=initial_date_param,
        end_date=end_date_param,
        unit=unit_param
    ))