from django.db.models import Q
from web.models import User


class OtherHelper:

    @staticmethod
    def get_other(**filters):
        unit = filters['unit']

        q_unit = Q()
        if unit:
            unit_obj = User.objects.get(id=unit) if unit else None

        return list(
            User.objects.values_list('name', flat=True).filter(q_unit).order_by('id').distinct())