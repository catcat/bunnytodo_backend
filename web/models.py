from django.db import models


class User(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class UserTask(models.Model):
    user = models.ForeignKey('User', related_name='tasks', on_delete=models.CASCADE)
    description = models.CharField(max_length=550)
    TODO = 1
    DONE = 2
    STATE = (
        (TODO, 'To do'),
        (DONE, 'Done'),
    )
    task_state = models.PositiveSmallIntegerField(choices=STATE, default=TODO)

    def __str__(self):
        formato = 'id: {} user: {}'
        return formato.format(self.id, self.user.name)