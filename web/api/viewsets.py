from web.models import User, UserTask
from rest_framework.response import Response
from web.api import serializers
from rest_framework import viewsets
from rest_framework.decorators import action

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('id')
    serializer_class = serializers.UserSerializer

    @action(detail=True)
    def all_todo_byuser(self, request, pk=None):
        user = self.get_object() # retrieve an object by pk provided
        tasks = UserTask.objects.filter(user=user).order_by('id')
        tasks_json = serializers.UserTaskSerializer(tasks, many=True)
        return Response(tasks_json.data)


    @action(detail=False)
    def full_user(self, request):
        users = User.objects.all().prefetch_related('tasks')
        tasks_json = serializers.FullUserSerializer(users, many=True)
        return Response(tasks_json.data)


class UserTaskViewSet(viewsets.ModelViewSet):
    queryset = UserTask.objects.all().order_by('id')
    serializer_class = serializers.OnlyTaskSerializer
