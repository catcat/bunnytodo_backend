from web.models import User, UserTask
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id',
            'name',
        )

class UserTaskSerializer(serializers.ModelSerializer):
    user = UserSerializer(allow_null=True)

    class Meta:
        model = UserTask
        fields = (
            'id',
            'user',
            'description',
            'task_state',
        )

class TaskWithoutUSerSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserTask
        fields = (
            'id',
            'description',
            'task_state',
        )

class FullUserSerializer(serializers.ModelSerializer):
    tasks = TaskWithoutUSerSerializer(allow_null=True, many=True)
    class Meta:
        model = User
        fields = (
            'id',
            'name',
            'tasks'
        )

class OnlyTaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserTask
        fields = (
            'id',
            'user',
            'description',
            'task_state',
        )

    def create(self, validated_data):
        user = validated_data.pop('user')
        usertask_instance = UserTask.objects.create(**validated_data, user=user)
        return usertask_instance