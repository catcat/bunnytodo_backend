from django.contrib import admin
from .models import User, UserTask


class UserTaskInline(admin.TabularInline):
    model = UserTask
    extra = 1


class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    list_filter = (
        ('name'),
    )
    inlines = (UserTaskInline,)


admin.site.register(User, UserAdmin)
admin.site.register(UserTask)