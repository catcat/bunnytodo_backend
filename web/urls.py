from django.urls import path, include
from .views import get_other
from web.api.routers import router

urlpatterns = [
    path('', include(router.urls)),
    path('other/', get_other), #Endpoints no relacionados al api
]